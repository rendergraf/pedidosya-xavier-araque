import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

const cookieparser = process.server ? require('cookieparser') : undefined

export const state = () => {
  return {
    auth: null,
    listaDeNegocios: null
  }
}

export const vuexLocal = new VuexPersistence({
  key: 'vuex'
})

export const mutations = {
  setNegocios(state, listaDeNegocios) {
    state.listaDeNegocios = listaDeNegocios
  },
  setAuth(state, auth) {
    state.auth = auth
  }
}
export const actions = {
  nuxtServerInit({ commit }, { req }) {
    let auth = null
    if (req.headers.cookie) {
      const parsed = cookieparser.parse(req.headers.cookie)
      try {
        auth = JSON.parse(parsed.auth)
      } catch (err) {
        // No valid cookie found
        return err
      }
    }
    commit('setAuth', auth)
  },
  /*   preserveLocal: (context) => {
    localStorage.setItem(
      'listaDeNegocios',
      JSON.stringify(context.state.listaDeNegocios)
    )
  }, */
  retrieveLocal: (context) => {
    /*     if (localStorage.getItem('listaDeNegocios') !== null) {
      let listaDeNegocios = JSON.parse(localStorage.getItem('listaDeNegocios'))
      context.commit('setNegocios', listaDeNegocios)
    } */
    context.commit('Algo')
  }
}
