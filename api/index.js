// INDEX.JS
const express = require('express')
const app = express()
const axios = require('axios')

app.get('/', (req, res, next) => {
  res.send('API root')
})

/* app.get('/random-movie', async (req, res, next) => {
  //const axios = require('axios')
  // Credenciales para autenticar la APP
  const client = 'test'
  const password = 'PeY@@Tr1v1@943'
  const pathAPI = 'http://localhost:3000/api/v1/'

  // Obtiene las credenciales de la APP
  const appkey = await axios.get(`${pathAPI}tokens?clientId=${client}&clientSecret=${password}`)

  res.json(appkey.data)

  // Guarda las credenciales en una variable
  let keyapp = appkey.data
  console.log('Credencial ' + appkey.data.access_token)
}) */

app.get('/autho', async (req, res) => {
  const API_AUTH_ACCESSTOKEN_URL = 'http://localhost:3000/api/v1/tokens'
  const CLIENT_ID = 'test'
  const CLIENT_SECRET = 'PeY@@Tr1v1@943'
  try {
    const { data } = await axios({
      method: 'get',
      url: API_AUTH_ACCESSTOKEN_URL,
      params: {
        clientId: CLIENT_ID,
        clientSecret: CLIENT_SECRET
      }
    })
    //res.send(key)
    appKeyToken = data
    console.log('Credencial en el servidor ' + appKeyToken.access_token)
  } catch (err) {
    if (err.response && err.response.status === 403) {
      res.sendStatus(err.response.status)
    }
    if (err.response && err.response.status === 504) {
      res.sendStatus(err.response.status)
    }
    console.error(err.message)
  }
})

const genToken = async () => {
  const API_AUTH_ACCESSTOKEN_URL = 'http://localhost:3000/api/v1/tokens'
  const CLIENT_ID = 'test'
  const CLIENT_SECRET = 'PeY@@Tr1v1@943'
  try {
    const { data } = await axios({
      method: 'get',
      url: API_AUTH_ACCESSTOKEN_URL,
      params: {
        clientId: CLIENT_ID,
        clientSecret: CLIENT_SECRET
      }
    })
    //res.send(key)
    appKeyToken = data
    console.log('Token Registro de APP ' + appKeyToken.access_token)
    return appKeyToken.access_token
  } catch (err) {
    if (err.response && err.response.status === 403) {
      res.sendStatus(err.response.status)
    }
    if (err.response && err.response.status === 504) {
      res.sendStatus(err.response.status)
    }
    //console.error(err.message)
  }
}

const genTokenUser = async (userName, passSecret, getTokenApp) => {
  const API_AUTH_ACCESSTOKEN_URL = 'http://localhost:3000/api/v1/tokens'
  const CLIENT_ID = decodeURIComponent(userName)
  const CLIENT_SECRET = `${passSecret}`
  const TOKEN_APP = getTokenApp
  //console.log('User ' + CLIENT_ID + ' Passs ' + CLIENT_SECRET + ' APP ' + TOKEN_APP)
  try {
    const { data } = await axios({
      method: 'get',
      url: API_AUTH_ACCESSTOKEN_URL,
      params: {
        userName: CLIENT_ID,
        password: CLIENT_SECRET
      },
      'Cache-Control': 'no-cache',
      Accept: '*/*',
      headers: { Authorization: TOKEN_APP }
    })

    //res.send(key)
    userToken = data
    //console.log('Token ' + userToken.access_token)
    return userToken
  } catch (err) {
    if (err.response && err.response.status === 403) {
      //res.sendStatus(err.response.status)
    }
    if (err.response && err.response.status === 504) {
      //res.sendStatus(err.response.status)
    }
    console.error(err.message)
    console.log(err.request._header)
  }
}

// Vamos a hacer el login
app.get('/login', async (req, res, next) => {
  let user = req.query['clientId']
  let pass = req.query['clientSecret']
  //console.log('User: ' + req.query['clientId'])
  //console.log('Password: ' + req.query['clientSecret'])
  // Generar el toke de la APP
  respuesta = async () => {
    try {
      let getToken = await genToken()
      //console.log('Token de APP ' + getToken)
      let getUser = await genTokenUser(user, pass, getToken)
      console.log('Token del User ' + getUser.access_token)
      res.send(getUser.access_token)
    } catch (err) {
      console.error(err.message)
    }
  }
  respuesta()
  //console.log(req)
  //res.send(data)
})

// export the server middleware
module.exports = {
  path: '/api',
  handler: app
}
